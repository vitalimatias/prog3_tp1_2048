package Clases;

/**
 * @author Matias
 *
 */
public class MatrizCuadricula {
	private int filas, columnas;
	protected Cuadricula[][] matriz;

	public MatrizCuadricula(int cantFilas, int cantColumnas) {
		filas = cantFilas;
		columnas = cantColumnas;		
		matriz = new Cuadricula[filas][columnas];
		for (int f = 0; f < filas; f++) {
			for (int c = 0; c < columnas; c++) {
				matriz[f][c] = new Cuadricula(0);
			}
		}
	}
	
	public int getInfoCuadricula(int fila, int columna){
		return matriz[fila][columna].getInfoNumero();
	}

	protected void agregarCuadricula(int valor, int posFila, int posColumna) {
		matriz[posFila][posColumna].agregarValor(valor);
		matriz[posFila][posColumna].activar();
	}

	protected void eliminarCuadricula(int posFila, int posColumna) {
		matriz[posFila][posColumna].cambiarValor(0);
		matriz[posFila][posColumna].desactivar();
	}

	private int valorRandom() {
		int valor = (int) (Math.random() * 4 + 0);
		if (valor == 0 || valor == 1)
			valor = 2;
		if (valor == 3)
			valor = 4;
		return valor;
	}

	private int[] posicionRandom() {
		boolean condicion = true;
		int posFila = -1;
		int posColumna = -1;
		while (condicion) {
			posFila = (int) (Math.random() * 4 + 0);
			posColumna = (int) (Math.random() * 4 + 0);
			if (!matriz[posFila][posColumna].estaActiva())
				condicion = false;
		}
		return new int[] { posFila, posColumna };
	}
	
	public void agregarCuadriculaRandom(){
		int [] posiciones = posicionRandom();
		agregarCuadricula(valorRandom(), posiciones[0], posiciones[1]);
	}
}
