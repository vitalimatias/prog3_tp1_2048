package UI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.Color;
import javax.swing.UIManager;

import Clases.ManejoMatriz;
import Clases.MatrizCuadricula;

import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.security.Key;

import javax.swing.JTextField;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class ventanaJuego {

	private JFrame frame;
	private JTextPane[][] matrizTextPane;
	private ManejoMatriz matrizCuadriculas;
	private JTextPane infoJugador;
	private JTextPane infoPuntaje;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ventanaJuego window = new ventanaJuego();
					window.frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	/**
	 * Create the application.
	 */
	public ventanaJuego() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent key) {
				if (key.getKeyCode() == key.VK_8) {
					matrizCuadriculas.moverArriba();
					refrescarVentana();
				}
			}
		});

		frame.getContentPane().setBackground(new Color(0, 255, 153));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JTextPane textPane = new JTextPane();
		textPane.setBackground(UIManager.getColor("Button.light"));
		textPane.setFont(new Font("Impact", Font.PLAIN, 22));
		textPane.setEditable(false);
		textPane.setText("2048");
		textPane.setBounds(187, 11, 57, 34);
		frame.getContentPane().add(textPane);

		JTextPane cuadricula00 = new JTextPane();
		cuadricula00.setFont(new Font("Impact", Font.PLAIN, 28));
		cuadricula00.setEditable(false);
		cuadricula00.setBounds(27, 55, 40, 40);
		frame.getContentPane().add(cuadricula00);

		JTextPane cuadricula01 = new JTextPane();
		cuadricula01.setFont(new Font("Impact", Font.PLAIN, 28));
		cuadricula01.setEditable(false);
		cuadricula01.setBounds(77, 55, 40, 40);
		frame.getContentPane().add(cuadricula01);

		JTextPane cuadricula02 = new JTextPane();
		cuadricula02.setFont(new Font("Impact", Font.PLAIN, 28));
		cuadricula02.setEditable(false);
		cuadricula02.setBounds(127, 55, 40, 40);
		frame.getContentPane().add(cuadricula02);

		JTextPane cuadricula03 = new JTextPane();
		cuadricula03.setFont(new Font("Impact", Font.PLAIN, 28));
		cuadricula03.setEditable(false);
		cuadricula03.setBounds(177, 56, 40, 40);
		frame.getContentPane().add(cuadricula03);

		JTextPane cuadricula10 = new JTextPane();
		cuadricula10.setFont(new Font("Impact", Font.PLAIN, 28));
		cuadricula10.setEditable(false);
		cuadricula10.setBounds(27, 106, 40, 40);
		frame.getContentPane().add(cuadricula10);

		JTextPane cuadricula11 = new JTextPane();
		cuadricula11.setFont(new Font("Impact", Font.PLAIN, 28));
		cuadricula11.setEditable(false);
		cuadricula11.setBounds(77, 106, 40, 40);
		frame.getContentPane().add(cuadricula11);

		JTextPane cuadricula12 = new JTextPane();
		cuadricula12.setFont(new Font("Impact", Font.PLAIN, 28));
		cuadricula12.setEditable(false);
		cuadricula12.setBounds(127, 106, 40, 40);
		frame.getContentPane().add(cuadricula12);

		JTextPane cuadricula13 = new JTextPane();
		cuadricula13.setFont(new Font("Impact", Font.PLAIN, 28));
		cuadricula13.setEditable(false);
		cuadricula13.setBounds(177, 107, 40, 40);
		frame.getContentPane().add(cuadricula13);

		JTextPane cuadricula20 = new JTextPane();
		cuadricula20.setFont(new Font("Impact", Font.PLAIN, 28));
		cuadricula20.setEditable(false);
		cuadricula20.setBounds(27, 157, 40, 40);
		frame.getContentPane().add(cuadricula20);

		JTextPane cuadricula21 = new JTextPane();
		cuadricula21.setFont(new Font("Impact", Font.PLAIN, 28));
		cuadricula21.setEditable(false);
		cuadricula21.setBounds(77, 157, 40, 40);
		frame.getContentPane().add(cuadricula21);

		JTextPane cuadricula22 = new JTextPane();
		cuadricula22.setFont(new Font("Impact", Font.PLAIN, 28));
		cuadricula22.setEditable(false);
		cuadricula22.setBounds(127, 157, 40, 40);
		frame.getContentPane().add(cuadricula22);

		JTextPane cuadricula23 = new JTextPane();
		cuadricula23.setFont(new Font("Impact", Font.PLAIN, 28));
		cuadricula23.setEditable(false);
		cuadricula23.setBounds(177, 158, 40, 40);
		frame.getContentPane().add(cuadricula23);

		JTextPane cuadricula30 = new JTextPane();
		cuadricula30.setFont(new Font("Impact", Font.PLAIN, 28));
		cuadricula30.setEditable(false);
		cuadricula30.setBounds(27, 208, 40, 40);
		frame.getContentPane().add(cuadricula30);

		JTextPane cuadricula31 = new JTextPane();
		cuadricula31.setFont(new Font("Impact", Font.PLAIN, 28));
		cuadricula31.setEditable(false);
		cuadricula31.setBounds(77, 208, 40, 40);
		frame.getContentPane().add(cuadricula31);

		JTextPane cuadricula32 = new JTextPane();
		cuadricula32.setFont(new Font("Impact", Font.PLAIN, 28));
		cuadricula32.setEditable(false);
		cuadricula32.setBounds(127, 208, 40, 40);
		frame.getContentPane().add(cuadricula32);

		JTextPane cuadricula33 = new JTextPane();
		cuadricula33.setFont(new Font("Impact", Font.PLAIN, 28));
		cuadricula33.setEditable(false);
		cuadricula33.setBounds(177, 209, 40, 40);
		frame.getContentPane().add(cuadricula33);

		matrizTextPane = new JTextPane[4][4];
		matrizTextPane[0][0] = cuadricula00;
		matrizTextPane[0][1] = cuadricula01;
		matrizTextPane[0][2] = cuadricula02;
		matrizTextPane[0][3] = cuadricula03;
		matrizTextPane[1][0] = cuadricula10;
		matrizTextPane[1][1] = cuadricula11;
		matrizTextPane[1][2] = cuadricula12;
		matrizTextPane[1][3] = cuadricula13;
		matrizTextPane[2][0] = cuadricula20;
		matrizTextPane[2][1] = cuadricula21;
		matrizTextPane[2][2] = cuadricula22;
		matrizTextPane[2][3] = cuadricula23;
		matrizTextPane[3][0] = cuadricula30;
		matrizTextPane[3][1] = cuadricula31;
		matrizTextPane[3][2] = cuadricula32;
		matrizTextPane[3][3] = cuadricula33;

		matrizCuadriculas = new ManejoMatriz(4, 4);
		matrizCuadriculas.agregarCuadriculaRandom();
		matrizCuadriculas.agregarCuadriculaRandom();

		JTextPane txtpnJugador = new JTextPane();
		txtpnJugador.setFont(new Font("Tahoma", Font.BOLD, 14));
		txtpnJugador.setBackground(new Color(0, 255, 153));
		txtpnJugador.setEditable(false);
		txtpnJugador.setText("Jugador");
		txtpnJugador.setBounds(325, 55, 70, 20);
		frame.getContentPane().add(txtpnJugador);

		JTextPane txtpnPuntaje = new JTextPane();
		txtpnPuntaje.setFont(new Font("Tahoma", Font.BOLD, 14));
		txtpnPuntaje.setBackground(new Color(0, 255, 153));
		txtpnPuntaje.setEditable(false);
		txtpnPuntaje.setText("Puntaje");
		txtpnPuntaje.setBounds(325, 106, 70, 20);
		frame.getContentPane().add(txtpnPuntaje);

		infoJugador = new JTextPane();
		infoJugador.setFont(new Font("Tahoma", Font.BOLD, 14));
		infoJugador.setEditable(false);
		infoJugador.setBounds(293, 76, 130, 23);
		frame.getContentPane().add(infoJugador);

		infoPuntaje = new JTextPane();
		infoPuntaje.setFont(new Font("Impact", Font.PLAIN, 28));
		infoPuntaje.setEditable(false);
		infoPuntaje.setBounds(293, 126, 130, 40);
		frame.getContentPane().add(infoPuntaje);

		JButton btnArriba = new JButton("\u2191");
		btnArriba.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnArriba.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				matrizCuadriculas.moverArriba();
				matrizCuadriculas.agregarCuadriculaRandom();
				refrescarVentana();
			}
		});
		btnArriba.setBackground(UIManager.getColor("Button.darkShadow"));
		btnArriba.setBounds(307, 191, 53, 23);
		frame.getContentPane().add(btnArriba);

		JButton btnAbajo = new JButton("\u2193");
		btnAbajo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				matrizCuadriculas.moverAbajo();
				matrizCuadriculas.agregarCuadriculaRandom();
				refrescarVentana();
			}
		});
		btnAbajo.setBackground(UIManager.getColor("Button.darkShadow"));
		btnAbajo.setBounds(307, 225, 53, 23);
		frame.getContentPane().add(btnAbajo);

		JButton btnIzquieda = new JButton("\u2190");
		btnIzquieda.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				matrizCuadriculas.moverIzquierda();
				matrizCuadriculas.agregarCuadriculaRandom();
				refrescarVentana();
			}
		});
		btnIzquieda.setBackground(UIManager.getColor("Button.darkShadow"));
		btnIzquieda.setBounds(244, 225, 53, 23);
		frame.getContentPane().add(btnIzquieda);

		JButton btnDerecha = new JButton("\u2192");
		btnDerecha.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				matrizCuadriculas.moverDerecha();
				matrizCuadriculas.agregarCuadriculaRandom();
				refrescarVentana();
			}
		});
		btnDerecha.setBackground(UIManager.getColor("Button.darkShadow"));
		btnDerecha.setBounds(371, 225, 53, 23);
		frame.getContentPane().add(btnDerecha);
		
		
		refrescarVentana();
	}

	private void refrescarVentana() {
		String aux = "";
		for (int f = 0; f < 4; f++) {
			for (int c = 0; c < 4; c++) {
				if (matrizCuadriculas.getInfoCuadricula(f, c) == 0)
					aux = "";
				else
					aux = String.valueOf(matrizCuadriculas.getInfoCuadricula(f, c));
				matrizTextPane[f][c].setText(aux);
			}
		}
		aux = String.valueOf(matrizCuadriculas.sumarValoresCuadriculas());
		infoPuntaje.setText(aux);
	}
}
