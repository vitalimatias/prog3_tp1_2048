package Clases;

public class Juego {
	private int puntos;
	private String nombreJugador;
	private ManejoMatriz matrizJuego;
	
	
	public Juego(int puntos, String nombreJugador, int cantFilas, int cantColumnas) {
		this.puntos = puntos;
		this.nombreJugador = nombreJugador;
		this.matrizJuego = new ManejoMatriz(cantFilas, cantColumnas);
		matrizJuego.agregarCuadriculaRandom();
		matrizJuego.agregarCuadriculaRandom();
		puntos = matrizJuego.sumarValoresCuadriculas();
	}
	
	public void seMueveArriba(){
		matrizJuego.moverArriba();
		puntos = matrizJuego.sumarValoresCuadriculas();
	}
	
	public void seMueveAbajo(){
		matrizJuego.moverAbajo();
		puntos = matrizJuego.sumarValoresCuadriculas();
	}
	
	public void seMueveIzquierda(){
		matrizJuego.moverIzquierda();
		puntos = matrizJuego.sumarValoresCuadriculas();
	}
	
	public void seMueveDerecha(){
		matrizJuego.moverDerecha();
		puntos = matrizJuego.sumarValoresCuadriculas();
	}
	
	public boolean puedeSeguir(){
		return matrizJuego.existePosicionVacia() && !matrizJuego.seLlegoA2048();
	}
	
	public void gameOver(){
		//Agregar manejo de archivo para que guarde el nombre del jugador y el puntaje.
	}

}
