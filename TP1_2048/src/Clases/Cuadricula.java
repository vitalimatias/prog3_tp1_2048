package Clases;

public class Cuadricula {
	private int infoNumero;
	private boolean estaActiva;

	public Cuadricula(int numero) {
		infoNumero = numero;
		estaActiva = false;		
	}

	public int getInfoNumero() {
		return infoNumero;
	}
	
	public boolean estaActiva(){
		return estaActiva;
	}

	public void agregarValor(int valor) {
		infoNumero += valor;
	}

	public void cambiarValor(int valor) {
		infoNumero = valor;
	}

	public void activar() {
		estaActiva = true;
	}
	
	public void desactivar(){
		estaActiva = false;
	}
	
	public boolean equals(Cuadricula cuadricula){
		return infoNumero == cuadricula.getInfoNumero();
	}
}
