package Clases;

import org.junit.validator.PublicClassValidator;

public class ManejoMatriz extends MatrizCuadricula {
	private int filas, columnas;
	private Cuadricula[][] matriz;

	public ManejoMatriz(int cantFilas, int cantColumnas) {
		super(cantFilas, cantColumnas);
		matriz = super.matriz;
		filas = cantFilas;
		columnas = cantColumnas;
	}

	/**
	 * "Desde" hace referencia a posicion inicial del movimiento "Hasta" hace
	 * referencia a posicion final del movimiento
	 */
	private void moverCuadricula(int posFilaDesde, int posColumnaDesde, int posFilaHasta, int posColumnaHasta) {
		matriz[posFilaHasta][posColumnaHasta].agregarValor(matriz[posFilaDesde][posColumnaDesde].getInfoNumero());
		eliminarCuadricula(posFilaDesde, posColumnaDesde);
	}

	public void moverArriba() {
		for (int f = 1; f < filas; f++) {
			for (int c = 0; c < columnas; c++) {
				int fila = f;
				boolean condicion = true;
				while (condicion){
					if (!matriz[fila - 1][c].estaActiva()){
						moverCuadricula(fila, c, fila - 1, c);
						if (fila>1)
							fila--;
						else
							condicion = false;
					}
					else if (matriz[fila - 1][c].equals(matriz[fila][c])){
						moverCuadricula(fila, c, fila - 1, c);
						condicion = false;
					}
					else condicion = false;					
				}
			}
		}
	}

	public void moverAbajo() {
		for (int f = 0; f < filas - 1; f++) {
			for (int c = 0; c < columnas; c++) {
				int fila = f;
				boolean condicion = true;
				while (condicion){
					if (!matriz[fila + 1][c].estaActiva()){
						moverCuadricula(fila, c, fila + 1, c);
						if (fila < filas-2) fila++;
						else condicion = false;
					}
					else if (matriz[fila + 1][c].equals(matriz[fila][c])){
						moverCuadricula(fila, c, fila + 1, c);			
						condicion = false;
					}
					else condicion = false;					
				}				
			}
		}
	}

	public void moverIzquierda() {
		for (int f = 0; f < filas; f++) {
			for (int c = 1; c < columnas; c++) {
				int columna = c;
				boolean condicion = true;
				while (condicion){
					if (!matriz[f][columna - 1].estaActiva()){
						moverCuadricula(f, columna, f, columna - 1);
						if (columna>1)
							columna--;
						else
							condicion = false;
					}
					else if (matriz[f][c-1].equals(matriz[f][columna])){
						moverCuadricula(f, columna, f, columna);
						condicion = false;
					}
					else condicion = false;
					
				}
			}
		}
	}

	public void moverDerecha() {
		for (int f = 0; f < filas; f++) {
			for (int c = 0; c < columnas-1; c++) {
				int columna = c;
				boolean condicion = true;
				while (condicion){
					if (!matriz[f][columna + 1].estaActiva()){
						moverCuadricula(f, columna, f, columna + 1);
						if (columna>columnas-1)
							columna++;
						else
							condicion = false;
					}
					else if (matriz[f][c+1].equals(matriz[f][columna])){
						moverCuadricula(f, columna, f, columna);
						condicion = false;
					}
					else condicion = false;
					
				}
			}
		}
	}

	public int sumarValoresCuadriculas() {
		int sumaTotal = 0;
		for (int f = 0; f < filas; f++) {
			for (int c = 0; c < columnas; c++) {
				sumaTotal += matriz[f][c].getInfoNumero();
			}
		}
		return sumaTotal;
	}

	public boolean existePosicionVacia() {
		for (int f = 0; f < filas; f++) {
			for (int c = 0; c < columnas; c++) {
				if (!matriz[f][c].estaActiva())
					return true;
			}
		}
		return false;
	}

	public boolean seLlegoA2048() {
		for (int f = 0; f < filas; f++) {
			for (int c = 0; c < columnas; c++) {
				if (matriz[f][c].getInfoNumero() == 2048)
					return true;
			}
		}
		return false;
	}
}
